
# ToDo Manual Script Suite Test

Implementing Manual script Test suite for TODO site, using Python+Selenium as the platform.

## Installation

You will need to have the below installed in your system

```
Pythonn 3.8
Executable of Chrome webdriver 83 (chromewebdriver.exe) or up copied in the scripts directory of Python interpreter
pip or PyCharm Community edition installed in the system
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install selenium and behave. Or, use the PyCharm IDE to install the below in your selected interpreter:

```bash
pip install selenium
```

## Usage

Open terminal inside directory of this project (./todo_automation_suite/)

```
python script.py
```