from telnetlib import EC

import selenium
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait

driver = webdriver.Chrome()
driver.implicitly_wait(10)


url = 'http://localhost:3000/'
driver.get(url)
'''
group_ui_element: WebElement = driver.find_element_by_xpath('//*[@id="root"]/div/ul')

group_li_elements: [WebElement] = None
try:
    group_li_elements = group_ui_element.find_elements_by_xpath('.//li')
except:
    group_li_elements = None
'''


#get initial set of todos
elements: [WebElement] = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
todos = []
for element in elements:
    todos.append({
        'todo': element.find_element_by_xpath('.//div').get_attribute('innerText'),
        'state': '[X]' == element.find_element_by_xpath('.//span').get_attribute('innerText')
    })

#add a to do by clicking + sign, it should appear at the top of the list of li elements with [ ]
#size of list must have inscreased by 1
driver.find_element_by_xpath('/html/body/div/div/form/input').send_keys('new todo 1')
driver.find_element_by_xpath('/html/body/div/div/form/button').click()
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
if len(todos) + 1 == len(elements):
    print(True)
else:
    print(False)
if elements[0].find_element_by_xpath('.//div').get_attribute('innerText') == 'new todo 1' and\
        elements[0].find_element_by_xpath('.//span').get_attribute('innerText') == '[ ]':
    print(True)
else:
    print(False)

#select new to do 1 before adding another to do, and check if it shows at the bottom of the list
# with [X] instead of [ ]
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
elements[0].find_element_by_xpath('.//span').click()
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
if elements[len(elements)-1].find_element_by_xpath('.//span').get_attribute('innerText') == '[X]'\
        and elements[len(elements)-1].find_element_by_xpath('.//div').get_attribute('innerText') == 'new todo 1':
    print(True)
else:
    print(False)

#update todos
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
todos = []
for element in elements:
    todos.append({
        'todo': element.find_element_by_xpath('.//div').get_attribute('innerText'),
        'state': '[X]' == element.find_element_by_xpath('.//span').get_attribute('innerText')
    })

#add a to do by pressing enter, it should appear at the top of the list of li elements with [ ]
#size of list must have inscreased by 1
driver.find_element_by_xpath('/html/body/div/div/form/input').send_keys('new todo 2\n')
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
if len(todos) + 1 == len(elements):
    print(True)
else:
    print(False)
if elements[0].find_element_by_xpath('.//div').get_attribute('innerText') == 'new todo 2':
    print(True)
else:
    print(False)

#check to see if the new to do 1 was still selected or not at the last position in the list
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
if elements[len(elements) - 1].find_element_by_xpath('.//span').get_attribute('innerText') == '[X]' and\
        elements[len(elements) - 1].find_element_by_xpath('.//div').get_attribute('innerText') == 'new todo 1':
    print(True)
else:
    print(False)

#search for buy flowers to do and uncheck it, it should come at the top with [ ]
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
for element in elements:
    if element.find_element_by_xpath('.//div').get_attribute('innerText') == 'buy flowers':
        element.find_element_by_xpath('.//span').click()
        break
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
if elements[0].find_element_by_xpath('.//div').get_attribute('innerText') == 'buy flowers' and\
    elements[0].find_element_by_xpath('.//span').get_attribute('innerText') == '[ ]':
    print(True)
else:
    print(False)

#update todos
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
todos = []
for element in elements:
    todos.append({
        'todo': element.find_element_by_xpath('.//div').get_attribute('innerText'),
        'state': '[X]' == element.find_element_by_xpath('.//span').get_attribute('innerText')
    })

#delete to do #2 and the size of the list should decrease by 1, and the deleted element should not be there anymore
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
deleted_todo = elements[1].find_element_by_xpath('.//div').get_attribute('innerText')
elements[1].find_element_by_xpath('.//button').click()
elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
if len(todos) - 1 == len(elements):
    print(True)
else:
    print(False)
result = True
for element in elements:
    if element.find_element_by_xpath('.//div').get_attribute('innerText') == deleted_todo:
        result = False
        break
print(result)

#We can delete all to dos and the list will become empty or non existent
try:
    elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
except:
    print('error retrieving the list of todos')
items_to_be_deleted = len(elements)
for i in range(0, items_to_be_deleted):
    try:
        elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
        print('element being deleted is ' + elements[i - 1].find_element_by_xpath('.//div').get_attribute('innerText'))
        elements[i-1].find_element_by_xpath('.//button').click()
    except:
        print('error retrieving or cancelling the todos')
try:
    elements = driver.find_elements_by_xpath('/html/body/div/div/ul/li')
    if elements is not None and len(elements) > 0:
        print(False)
        for element in elements:
            print(element.find_element_by_xpath('.//div').get_attribute('innerText'))
    else:
        print(True)
except:
    print('error retrieving the list of todos')
    print(True)





'''
driver.get(url)
driver.find_element_by_xpath(element_xpath)
element.find_element_by_xpath(element_xpath)
element: WebElement = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, element_xpath)))
driver.find_element_by_id(element_id)
driver.find_element_by_name(element_name)
driver.find_element_by_css_selector(element_css_selector)
element.click()
driver.execute_script("arguments[0].click();", element)
ActionChains(self._driver).move_to_element(element).send_keys(keys_to_be_sent).perform()
element.send_keys(keys_to_be_sent)
driver.execute_script("arguments[0].scrollIntoView(true);", element);
ActionChains(self._driver).move_to_element(element).send_keys(Keys.CONTROL, "a", Keys.DELETE).perform()
element.send_keys(Keys.CONTROL, "a", Keys.DELETE)
element.text
element.get_attribute(attribute)
element.get_attribute(attribute).__str__().strip().lower()
driver.current_url
WebDriverWait(driver, 10).until(EC.url_changes(driver.current_url))
driver.current_url.__str__().startswith(expected_url)
'''


driver.quit()