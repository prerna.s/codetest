Feature: Play with the todo task list
  Background: Landing on the TODO site
    Given The user tries to open the TODO site
    And user sees the correct URL and title "React App" of TODO site

  Scenario: Adding a todo
    Given user enters "new todo 1" as a new todo on TODO site
    When user clicks plus on TODO site
    Then user sees "new todo 1" unchecked on top of list on TODO site

  Scenario: Checking a todo
    Given user sees "new todo 1" unchecked
    When user checks "new todo 1"
    Then user sees "new todo 1" checked at the bottom of list on TODO site