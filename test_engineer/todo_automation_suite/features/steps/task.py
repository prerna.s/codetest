from behave import *
import time

from selenium.webdriver.remote.webelement import WebElement

use_step_matcher('parse')


@given(u'The user tries to open the TODO site')
def step_user_opens_todo_site(context):
    if not context.commonOperations.url_as_expected(context.url):
        assert(context.commonOperations.open_url(context.url))


@given(u'user sees the correct URL and title "{title}" of TODO site')
def step_user_sees_title_and_url_of_todo_site(context, title):
    assert(context.commonOperations.url_as_expected(context.url))
    assert(context.commonOperations.title_as_expected(title))


@given(u'user enters "{todo_name}" as a new todo on TODO site')
def step_user_types_new_todo_on_todo_site(context, todo_name):
    elements: [WebElement] = context.commonOperations.get_xpath_elements_or_none('/html/body/div/div/ul/li')
    context.commonOperations.todos = []
    for element in elements:
        context.commonOperations.todos.append({
            'todo': context.commonOperations.get_nested_xpath_element_attribute('.//div', element, 'innerText'),
            'state': '[X]' == context.commonOperations.get_nested_xpath_element_attribute('.//span', element, 'innerText')
        })
    assert(context.commonOperations.get_xpath_element_and_send_keys('/html/body/div/div/form/input', 'new todo 1'))


@when(u'user clicks plus on TODO site')
def step_user_clicks_plus_on_todo_site(context):
    assert(context.commonOperations.get_xpath_element_and_click('/html/body/div/div/form/button'))


@then(u'user sees "{todo_name}" unchecked on top of list on TODO site')
def step_user_sees_new_todo_appropriately_on_todo_site(context, todo_name):
    elements = context.commonOperations.get_xpath_elements_or_none('/html/body/div/div/ul/li')
    assert(len(context.commonOperations.todos) + 1 == len(elements))
    assert(context.commonOperations.nested_xpath_element_exists_with_attribute('.//div', elements[0], 'innerText', 'new todo 1', False) and\
            context.commonOperations.nested_xpath_element_exists_with_attribute('.//span', elements[0], 'innerText', '[ ]', False))


@given(u'user sees "{todo}" unchecked')
def step_impl(context, todo):
    elements: [WebElement] = context.commonOperations.get_xpath_elements_or_none('/html/body/div/div/ul/li')
    todo_found = False
    for element in elements:
        if context.commonOperations.nested_xpath_element_exists_with_attribute('.//div', elements[0], 'innerText', 'new todo 1', False) and\
            context.commonOperations.nested_xpath_element_exists_with_attribute('.//span', elements[0], 'innerText', '[ ]', False):
            todo_found = True
            break
    assert(todo_found)


@when(u'user checks "{todo}"')
def step_impl(context, todo):
    elements: [WebElement] = context.commonOperations.get_xpath_elements_or_none('/html/body/div/div/ul/li')
    element_clicked = False
    for element in elements:
        if context.commonOperations.nested_xpath_element_exists_with_attribute('.//div', elements[0], 'innerText',
                                                                               'new todo 1', False) and \
                context.commonOperations.nested_xpath_element_exists_with_attribute('.//span', elements[0], 'innerText',
                                                                                    '[ ]', False):
            assert(context.commonOperations.get_nested_xpath_element_and_click(element, './/span'))
            element_clicked = True
            break
    assert(element_clicked)

@then(u'user sees "{todo}" checked at the bottom of list on TODO site')
def step_impl(context, todo):
    elements = context.commonOperations.get_xpath_elements_or_none('/html/body/div/div/ul/li')
    assert (len(context.commonOperations.todos) + 1 == len(elements))
    assert (context.commonOperations.nested_xpath_element_exists_with_attribute('.//div', elements[len(elements)-1], 'innerText',
                                                                                'new todo 1', False) and \
            context.commonOperations.nested_xpath_element_exists_with_attribute('.//span', elements[len(elements)-1], 'innerText',
                                                                                '[X]', False))
    elements: [WebElement] = context.commonOperations.get_xpath_elements_or_none('/html/body/div/div/ul/li')
    context.commonOperations.todos = []
    for element in elements:
        context.commonOperations.todos.append({
            'todo': context.commonOperations.get_nested_xpath_element_attribute('.//div', element, 'innerText'),
            'state': '[X]' == context.commonOperations.get_nested_xpath_element_attribute('.//span', element,
                                                                                          'innerText')
        })

